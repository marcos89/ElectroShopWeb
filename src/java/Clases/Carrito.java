/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;

/**
 *
 * @author Grupo2
 */
public class Carrito {
    
    private ArrayList<ProductoCarrito> carrito;
    
    public Carrito(ArrayList<ProductoCarrito> carrito){
        this.carrito = carrito;
    }
    
    public ArrayList<ProductoCarrito> getProductos(){
        return this.carrito;
    }
    
    public void setProductos(ArrayList<ProductoCarrito> carrito){
        this.carrito = carrito;
    }
    
    public float getPrecioTotal(){
        float precioTotal = 0;
        for(ProductoCarrito producto : this.carrito){
            precioTotal += producto.getPrecioCantidad();
        }
        precioTotal += 6;
        return precioTotal;
    }    
}
