/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Grupo2
 */
public class Pedido {
    
    private int idPedido;
    private int idUsuario;
    private Date fecha;
    private Carrito carrito;

    public Pedido() {
    }

    public Pedido(int idUsuario, Date fecha, Carrito carrito) {
        this.idUsuario = idUsuario;
        this.fecha = fecha;
        this.carrito = carrito;
    }

    public Pedido(int idPedido, int idUsuario, Date fecha, Carrito carrito) {
        this.idPedido = idPedido;
        this.idUsuario = idUsuario;
        this.fecha = fecha;
        this.carrito = carrito;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }
    
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Carrito getCarrito() {
        return carrito;
    }

    public void setCarrito(Carrito carrito) {
        this.carrito = carrito;
    }
    
}
