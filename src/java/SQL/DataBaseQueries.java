/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import Clases.Categoria;
import Clases.Carrito;
import Clases.Pedido;
import Clases.Producto;
import Clases.ProductoCarrito;
import Clases.ProductoPedido;
import Clases.Usuario;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupo2
 */
public class DataBaseQueries {

    private static Connection conn, conn2;
    private static Statement st, st2;
    private static ResultSet resultado, resultado2;

    private static String localurl = "jdbc:mysql://localhost:3306/electroshop_3ms";
    private static String remoteurl = "jdbc:mysql://node19800-electroshop.jelastic.cloudhosted.es/electroshop_3ms";
    private static String localpass = "root";
    private static String remotepass = "ESScbo38278";
    //usuario jetlastic mateo_bistuer@hotmail.com
    //contraseña jetlastic 3mjava
    // http://electroshop.jelastic.cloudhosted.es/
    private static String user = "root";
    private static String url = remoteurl;
    private static String pass = remotepass;
    //ESScbo38278

    public static Usuario selectUsuario(String mail) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT usuarios.*, roles.nombre as rol FROM usuarios, roles_usuario, roles WHERE usuarios.id = roles_usuario.idusuario AND roles_usuario.idrol = roles.id AND mail = '" + mail + "'";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            Usuario usuario = null;
            while (resultado.next()) {
                usuario = new Usuario(resultado.getInt("id"), resultado.getString("nombre"), resultado.getString("apellidos"), resultado.getString("direccion"), resultado.getString("telefono"), resultado.getString("mail"), resultado.getString("pass"), resultado.getString("rol"));
            }
            conn.close();
            return usuario;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void updateUsuario(Usuario usuario) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE usuarios SET "
                    + "nombre = \'" + usuario.getNombre()
                    + "',apellidos = '" + usuario.getApellidos()
                    + "',direccion = \'" + usuario.getDireccion()
                    + "',telefono = \'" + usuario.getTelefono()
                    + "',mail = \'" + usuario.getMail()
                    + "\' WHERE id = " + usuario.getId();
            System.out.println(query);
            st = conn.createStatement();
            st.executeUpdate(query);

            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public static boolean checkPass(int id, String currpass) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT * FROM usuarios WHERE id = "+id+" AND pass = '" + currpass + "'";
            System.out.println(sql);
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            boolean correctPass = false;
            while (resultado.next()) {
                correctPass = true;
            }
            conn.close();
            return correctPass;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public static void changePass(int id, String newpass) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE usuarios SET "
                    + "pass = '" + newpass
                    + "' WHERE id = " + id;
            System.out.println(query);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static int insertUsuario(Usuario usuario) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO usuarios VALUES("
                    + null + ", "
                    + "\"" + usuario.getNombre() + "\", "
                    + "\"" + usuario.getApellidos() + "\", "
                    + "\"" + usuario.getDireccion() + "\", "
                    + "\"" + usuario.getTelefono() + "\", "
                    + "\"" + usuario.getMail() + "\", "
                    + "\"" + usuario.getPass() + "\")";
            System.out.println(query);
            st = conn.createStatement();
            st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            int id = 0;
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            conn.close();
            DataBaseQueries.assignRol(id);
            return id;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static void assignRol(int idUsuario) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO roles_usuario VALUES("
                    + null + ", "
                    + idUsuario + ", "
                    + 2 + ")";
            System.out.println(query);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Producto> selectProductosAdmin() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT productos.*, categorias.nombre as categoria FROM productos, categorias WHERE productos.idcategoria = categorias.id ORDER BY productos.id";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            System.out.println(sql);
            ArrayList<Producto> productos = new ArrayList<>();
            while (resultado.next()) {

                Producto producto = new Producto(resultado.getInt("id"), resultado.getString("nombre"), resultado.getInt("precio"), resultado.getString("imagen"), resultado.getString("categoria"), resultado.getString("descripcion"), resultado.getInt("stock"), resultado.getInt("idusuario"));

                productos.add(producto);
            }
            conn.close();
            return productos;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
 
    public static ArrayList<Producto> selectProductos(String categoria) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT productos.*, categorias.nombre as categoria FROM productos, categorias WHERE productos.idcategoria = categorias.id AND categorias.nombre LIKE '"+categoria+"' ORDER BY productos.nombre";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Producto> productos = new ArrayList<>();
            while (resultado.next()) {

                Producto producto = new Producto(resultado.getInt("id"), resultado.getString("nombre"), resultado.getInt("precio"), resultado.getString("imagen"), resultado.getString("categoria"), resultado.getString("descripcion"), resultado.getInt("stock"), resultado.getInt("idusuario"));
                productos.add(producto);
            }
            conn.close();
            return productos;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     
    
     public static Producto selectProducto(int id) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT productos.*, categorias.nombre as categoria FROM productos, categorias where productos.idcategoria = categorias.id AND productos.id="+id;
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            System.out.println(sql);
            Producto producto = null;
            while (resultado.next()) {
             producto = new Producto(resultado.getInt("id"), resultado.getString("nombre"), resultado.getInt("precio"), resultado.getString("imagen"), resultado.getString("categoria"), resultado.getString("descripcion"), resultado.getInt("stock"), resultado.getInt("idusuario"));  
            }
            conn.close();
            return producto;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static int insertProducto(Producto producto) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO productos VALUES("
                    + null + ", "
                    + "\"" + producto.getNombre() + "\", "
                    + producto.getPrecio() + ", "
                    + "\"" + producto.getImagen() + "\","
                    + "\'" + producto.getCategoria() + "\',"
                    + "\"" + producto.getDescripcion() + "\","
                    + producto.getStock() + ","
                    + producto.getAdmin() + ")";
            st = conn.createStatement();
            System.out.println(query);
            int id = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            conn.close();
            return id;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static void updateProducto(Producto producto) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE productos SET "
                    + "nombre = \'" + producto.getNombre()
                    + "',precio = " + producto.getPrecio()
                    + ",imagen = \'" + producto.getImagen()
                    + "',idcategoria = '" + producto.getCategoria()
                    + "',descripcion = \'" + producto.getDescripcion()
                    + "',stock = " + producto.getStock()
                    + ",idusuario = " + producto.getAdmin()
                    + " WHERE id = " + producto.getId();
            st = conn.createStatement();
            System.out.println(query);
            st.executeUpdate(query);

            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void updateStock(int idProducto, int newStock) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE productos SET "
                    + "stock = " + newStock
                    + " WHERE id = " + idProducto;
            st = conn.createStatement();
            System.out.println(query);
            st.executeUpdate(query);

            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteProducto(int id) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "DELETE FROM productos WHERE id = " + id;
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {

            ex.printStackTrace();

        }
    }

    public static ArrayList<Pedido> selectPedidos() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT * FROM pedidos";
            System.out.println(sql);
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Pedido> pedidos = new ArrayList<>();
            while (resultado.next()) {
                Carrito carrito = DataBaseQueries.selectCarrito(resultado.getInt("id"));
                Pedido pedido = new Pedido(resultado.getInt("id"), resultado.getInt("idusuario"), resultado.getDate("fecha"), carrito);
                pedidos.add(pedido);
            }
            conn.close();
            return pedidos;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Pedido> selectPedidos(int idUsuario) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT * FROM pedidos WHERE idusuario = " + idUsuario;
            System.out.println(sql);
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Pedido> pedidos = new ArrayList<>();
            while (resultado.next()) {
                Carrito carrito = DataBaseQueries.selectCarrito(resultado.getInt("id"));
                Pedido pedido = new Pedido(resultado.getInt("id"), idUsuario, resultado.getDate("fecha"), carrito);
                pedidos.add(pedido);
            }
            conn.close();
            return pedidos;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Carrito selectCarrito(int idPedido) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn2 = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT * FROM productos_pedidos, productos WHERE productos_pedidos.idproducto = productos.id AND productos_pedidos.idpedido = "+resultado.getInt("id");
            System.out.println(sql);
            st2 = conn2.createStatement();
            resultado2 = st2.executeQuery(sql);
            ArrayList<ProductoCarrito> productos = new ArrayList<>();
            while (resultado2.next()) {
                Producto producto = new Producto(resultado2.getInt("id"), resultado2.getString("nombre"), resultado2.getFloat("precio"), resultado2.getInt("stock"));
                ProductoCarrito productoCarrito = new ProductoCarrito(producto, resultado2.getInt("cantidad"));
                productos.add(productoCarrito);
            }
            Carrito carrito = new Carrito(productos);
            conn2.close();
            return carrito;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static int insertPedido(Pedido pedido) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO pedidos VALUES("
                    + null + ", "
                    + pedido.getIdUsuario() + ", "
                    + "\"" + pedido.getFecha() + "\", "
                    + pedido.getCarrito().getPrecioTotal() + ")";
            System.out.println(query);
            st = conn.createStatement();
            st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            int id = 0;
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            conn.close();
            return id;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static void insertProductoPedido(int idPedido, ArrayList<ProductoCarrito> productos) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            for (ProductoCarrito producto : productos) {
                String query = "INSERT INTO productos_pedidos VALUES("
                        + null + ", "
                        + idPedido + ", "
                        + producto.getProducto().getId() + ", "
                        + producto.getCantidad() + ")";
                st = conn.createStatement();
                st.executeUpdate(query);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    //categorias

    public static int insertCategoria(Categoria categoria) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO categorias VALUES("
                    + null + ", "
                    + "\"" + categoria.getNombre() + "\")";
            st = conn.createStatement();
            System.out.println(query);
            int id = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            conn.close();
            return id;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static ArrayList<Categoria> selectCategorias() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT * FROM categorias";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Categoria> categorias = new ArrayList<>();
            while (resultado.next()) {

                Categoria categoria = new Categoria(resultado.getInt("id"), resultado.getString("nombre"));
                categorias.add(categoria);
            }
            conn.close();
            return categorias;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void updateCategoria(Categoria categoria) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE categorias SET "
                    + "nombre = \'" + categoria.getNombre()
                    + "' WHERE id = " + categoria.getId();
            st = conn.createStatement();
            System.out.println(query);
            st.executeUpdate(query);

            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void deleteCategoria(int id) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "DELETE FROM categorias WHERE id = " + id;
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        } catch (SQLException | ClassNotFoundException ex) {

            ex.printStackTrace();

        }
    }

}
