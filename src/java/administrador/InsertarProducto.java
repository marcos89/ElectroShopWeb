/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrador;

import SQL.DataBaseQueries;
import Clases.Producto;
import Clases.Usuario;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Grupo2
 */
@MultipartConfig
public class InsertarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        String relativeWebPath = "/";
        String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);

        Part filePart = request.getPart("imagen");
        OutputStream out = null;
        InputStream fileContent = null;
        int id = usuario.getId();
        String nombre = request.getParameter("nombre");
        float precio = Float.parseFloat(request.getParameter("precio"));
        String imagen;
        String categoria = request.getParameter("categoria");
        String descripcion = request.getParameter("descripcion");
        int stock = Integer.parseInt(request.getParameter("stock"));
        System.out.println(descripcion);

        if(categoria.equals("- Categorías -")){
            request.setAttribute("msg", "No se ha añadido el producto. No se ha seleccionado ninguna categoría.");
            ArrayList<Producto> productos = DataBaseQueries.selectProductosAdmin();
            request.setAttribute("productos", productos);
            RequestDispatcher rd = request.getRequestDispatcher("menuadmin.jsp");
            rd.forward(request, response);
        }else{
            if (descripcion.equals("")) {
                descripcion = "No hay descripción";
            }

            boolean exist = false;

            File file;

            if (filePart.getSize() == 0) {
                imagen = "imagenes/noimage.jpeg";
                Producto prod1 = new Producto(nombre, precio, imagen, categoria, descripcion, stock, id);

                try {
                    DataBaseQueries.insertProducto(prod1);
                    ArrayList<Producto> productos = DataBaseQueries.selectProductosAdmin();
                    request.setAttribute("productos", productos);
                        request.setAttribute("msg", "Se ha añadido un nuevo producto.");
                    RequestDispatcher rd = request.getRequestDispatcher("menuadmin.jsp");
                    rd.forward(request, response);
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(InsertarProducto.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
            } else {

                imagen = "imagenes/" + nombre + "." + filePart.getContentType().substring(6);

                System.out.println(filePart.getContentType().substring(6));


                file = new File(absoluteDiskPath + "/imagenes/", nombre + "." + filePart.getContentType().substring(6));

                if (file.exists()) {

                    request.setAttribute("msg", "Ya existe un archivo con este nombre");
                    ArrayList<Producto> productos = DataBaseQueries.selectProductosAdmin();
                    request.setAttribute("productos", productos);

                    exist = true;
                }

                if (!exist) {
                    out = new FileOutputStream(file);

                    fileContent = filePart.getInputStream();
                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = fileContent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

                    out.flush();

                    out.close();

                    fileContent.close();

                    Producto prod1 = new Producto(nombre, precio, imagen, categoria, descripcion, stock, id);

                    try {
                        DataBaseQueries.insertProducto(prod1);
                        ArrayList<Producto> productos = DataBaseQueries.selectProductosAdmin();
                        request.setAttribute("productos", productos);
                        request.setAttribute("msg", "Se ha añadido un nuevo producto.");
                        RequestDispatcher rd = request.getRequestDispatcher("menuadmin.jsp");
                        rd.forward(request, response);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(InsertarProducto.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                }

                RequestDispatcher rd = request.getRequestDispatcher("menuadmin.jsp");
                rd.forward(request, response);

            }
            
        }

    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertarProducto.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex) {
            Logger.getLogger(InsertarProducto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertarProducto.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex) {
            Logger.getLogger(InsertarProducto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
