/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compras;

import Clases.Carrito;
import Clases.Producto;
import Clases.ProductoCarrito;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Grupo2
 */
public class InsertarCarrito extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        System.out.println(request.getParameter("op"));
        HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(3600);
        Carrito carrito = null;
        ArrayList<ProductoCarrito> productos = null;
        if (request.getParameter("op").equals("insertar")) {
            int idproducto = Integer.parseInt(request.getParameter("id"));
            String nombre = request.getParameter("nombre");
            float precio = Float.parseFloat(request.getParameter("precio"));
            int stock = Integer.parseInt(request.getParameter("stock"));

            Producto producto = new Producto(idproducto, nombre, precio, stock);
            
            if (session.getAttribute("carrito") == null) {
                productos = new ArrayList<>();
                ProductoCarrito productoCarrito = new ProductoCarrito(producto, 1);
                productos.add(productoCarrito);
                carrito = new Carrito(productos);
                request.setAttribute("msg", "Has añadido un producto al carrito");
                session.setAttribute("carrito", carrito);
                RequestDispatcher rd = request.getRequestDispatcher("productos");
                rd.forward(request, response);
            } else {
                carrito = (Carrito) session.getAttribute("carrito");
                productos = carrito.getProductos();
                boolean productExists = false;
                boolean stockInsuficiente = false;
                for (ProductoCarrito prod : productos) {
                    if (prod.getProducto().getNombre().equals(producto.getNombre())) {
                        if(prod.getCantidad() == prod.getProducto().getStock()){
                            stockInsuficiente = true;
                            request.setAttribute("msg", "No quedan más existencias de este producto");
                            RequestDispatcher rd = request.getRequestDispatcher("productos");
                            rd.forward(request, response);
                        }else{
                            int nuevaCant = prod.getCantidad() + 1;
                            prod.setCantidad(nuevaCant);
                        }
                        productExists = true;
                    }
                }
                if (!productExists) {
                    ProductoCarrito productoCarrito = new ProductoCarrito(producto, 1);
                    productos.add(productoCarrito);
                }
                if(!stockInsuficiente){
                    carrito.setProductos(productos);
                    request.setAttribute("msg", "Has añadido un producto al carrito");
                    session.setAttribute("carrito", carrito);
                    RequestDispatcher rd = request.getRequestDispatcher("productos");
                    rd.forward(request, response);
                }
            }
        } else if(request.getParameter("op").equals("carrito")) {
            carrito = (Carrito) session.getAttribute("carrito");
            if(carrito != null){
                request.setAttribute("carrito", carrito);
                RequestDispatcher rd = request.getRequestDispatcher("vercarrito.jsp");
                rd.forward(request, response);
            }else{
                request.setAttribute("msg", "El carrito está vacío.");
                RequestDispatcher rd = request.getRequestDispatcher("productos");
                rd.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
