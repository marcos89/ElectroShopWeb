/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compras;

import Clases.Carrito;
import SQL.DataBaseQueries;
import Clases.Pedido;
import Clases.Producto;
import Clases.ProductoCarrito;
import Clases.ProductoPedido;
import Clases.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Grupo2
 */
public class RegistroPedido extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding( "UTF-8" );
        
        try {
            HttpSession session = request.getSession();
            
            Usuario usuario = (Usuario) session.getAttribute("usuario");
            int idusuario = usuario.getId();
            
            Carrito carrito = (Carrito) session.getAttribute("carrito");
            
            Date fecha = new Date(System.currentTimeMillis());
            Pedido pedido = new Pedido(idusuario, fecha, carrito);
            int idpedido = DataBaseQueries.insertPedido(pedido);
            
            ArrayList<ProductoCarrito> productos = carrito.getProductos();
            DataBaseQueries.insertProductoPedido(idpedido, productos);
            
            for(ProductoCarrito productoCarrito : productos){
                int newStock = productoCarrito.getProducto().getStock() - productoCarrito.getCantidad();
                DataBaseQueries.updateStock(productoCarrito.getProducto().getId(), newStock);
            }
            
            session.setAttribute("carrito", null);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(RegistroPedido.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
                 request.setAttribute("msg", "Tu pedido se ha realizado correctamente ");
        RequestDispatcher rd = request.getRequestDispatcher("perfilUsuario");
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
