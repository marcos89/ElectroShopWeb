<%-- 
    Document   : añadireditarproducto
    Created on : 28-dic-2017, 20:04:46
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroShop</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="background-color: azure">
        <% Usuario usuario = (Usuario) session.getAttribute("usuario");
           if(usuario == null){
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
           }
        %>
        <div class="container-fluid">
            <jsp:include page="jsps/header.jspf" flush="true"/>
            <nav   class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px;">Admin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

           
                  <li class="nav-item">
                        <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                    </li>
                <li class="nav-item">
                    <a class="nav-link" href="ListaCategorias" style="color: white;">Volver a categor&iacute;as</a>
                </li>
            
                <li class="nav-item">
                <%  
                    usuario = (Usuario) session.getAttribute("usuario");
                    if(usuario != null){
                %>
                    <a class="nav-link" href="Logout" style="color: white;">Logout</a>
             
                <%  
                    }
                %>
                </li>
            </ul>
                </div>
            </nav>
                
            <div style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">
            <%
                String id = "";
                String nombre = "";
               

                if (request.getParameter("id") == null) { %>

            <h1 style="text-align:center; padding: 10px; color: dodgerblue;">Insertar Categoria</h1>
            <form action="InsertarCategoria" method="post" accept-charset="UTF-8" style="margin-top: 30px;">
                <div class="form-group">
                    <label style="color: dodgerblue;">Nombre de categoria </label> <input  class="form-control"  type="text" name="nombre" required>
                </div>
          

                <% } else {

                    id = request.getParameter("id");
                    nombre = request.getParameter("nombre");
                  


                %>

                <h1 style="text-align:center; padding: 10px; color: dodgerblue;">Editar Categoria</h1>
                <form action="EditarCategoria" method="post" accept-charset="UTF-8" style="margin-top: 30px;">
                    <input type="hidden" name="id" value="<%= id%>">



                    <div class="form-group">
                        <label style="color: dodgerblue;">Nombre de la categoria</label> <input  class="form-control"  type="text" name="nombre"  value="<%= nombre%>" required>

                    </div>
                   




                    <%}%>

                    <div style="text-align: center; margin-top: 30px;">
                        <input class="btn btn-primary" style="width: 200px;" type="submit" > 
                    </div>
                </form>
            </div>        
                <jsp:include page="jsps/footer.jspf" flush="true"/> 

        </div>

       
        <jsp:include page="jsps/scripts.jspf" flush="true"/>
    </body>

</html>
