<%-- 
    Document   : añadireditarproducto
    Created on : 28-dic-2017, 20:04:46
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroShop</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="background-color: azure">
        <% Usuario usuario = (Usuario) session.getAttribute("usuario");
            if (usuario == null) {
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
            }
        %>
        <div class="container-fluid">
            <jsp:include page="jsps/header.jspf" flush="true"/>
                <nav   class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px;">Admin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                    <li class="nav-item">
                        <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ListaProductos" style="color: white;"> Volver a productos</a>
                    </li>

                    <li class="nav-item">
                        <%
                            usuario = (Usuario) session.getAttribute("usuario");
                            if (usuario != null) {
                        %>
                        <a class="nav-link" href="Logout" style="color: white;">Logout</a>

                        <%
                            }
                        %>
                    </li>
                </ul>
                </div>
            </nav>

            <div style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">
            <% if (request.getParameter("id") == null) { %>

            <h1 style="text-align:center; padding: 10px; color: dodgerblue;">Insertar producto</h1>
            <form action="InsertarProducto" method="post" accept-charset="UTF-8" enctype="multipart/form-data" >
                <div class="form-group">
                    <label style="color: dodgerblue;" for="nombre">Nombre del producto </label> <input id="nombre" class="form-control"  type="text" name="nombre" required>
                </div>
                <div class="form-group">
                    <label style="color: dodgerblue;" for="categoria">Categoria: </label> 
                    <select id="categoria" class="form-control" name="categoria">
                        <option> - Categorías - </option>
                        <c:forEach items="${categorias}" var="categoria">
                            <option value="${categoria.getId()}">${categoria.getNombre()}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label style="color: dodgerblue;" for="precio">Precio </label> <input id="precio" class="form-control" step="0.1" type="number" name="precio" min="0" required>
                </div>

                <div class="form-group">
                    <label style="color: dodgerblue;" for="descripcion">Descripci&oacute;n </label>
                    <textarea id="descripcion" class="form-control" name="descripcion" label="Descripción"  rows="10" ></textarea> 
                       
                </div>
                <div class="form-group">
                    <label style="color: dodgerblue;" for="stock">Stock </label> <input id="stock" class="form-control" step="1" type="number" name="stock" min="0" required>
                </div>   

                <div class="form-group">
                    <label style="color: dodgerblue;" for="customFile">Seleccione imagen</label>
                    <br>
                    <br>
                    <input type="file" name="imagen" class="form-control-file" id="customFile" 
                           accept=".jpeg, .png, .jpg">
                </div>

                    <% } else {

                    %>

                    <h1 style="text-align:center; padding: 30px; color: dodgerblue;">Editar producto</h1>
                    <form action="EditarProducto" method="post" accept-charset="UTF-8" enctype="multipart/form-data"  >
                        <input type="hidden" name="id" value="${producto.getId()}">
                        <input type="hidden" name="imgname" value="${producto.getImagen()}">
                        <div class="form-group">
                            <label style="color: dodgerblue;">Nombre del producto </label> <input  class="form-control"  type="text" name="nombre"  value="${producto.getNombre()}" required>

                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Categoria: </label> 
                            <select class="form-control" name="categoria">
                                <option disabled> - Categorías - </option>
                                <c:forEach items="${categorias}" var="categoria">
                                    <c:if test="${categoria.getNombre() == producto.getCategoria()}">
                                        <option value="${categoria.getId()}" selected>${categoria.getNombre()}</option>
                                    </c:if>
                                    <c:if test="${categoria.getNombre() != producto.getCategoria()}">
                                        <option value="${categoria.getId()}">${categoria.getNombre()}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Precio </label> <input  class="form-control" step="0.1" type="number" name="precio" min="0" value="${producto.getPrecio()}" required>

                        </div>

                        <div class="form-group">
                            <label style="color: dodgerblue;">Descripci&oacute;n </label>
                            <textarea class="form-control" name="descripcion" label="Descripción"  rows="10" >${producto.getDescripcion()}</textarea> 

                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Stock </label> <input  class="form-control" step="1" type="number" name="stock" min="0" value="${producto.getStock()}" required>

                        </div>     


                        <div class="form-group">
                            <label style="color: dodgerblue;" for="customFile">Seleccione imagen</label>
                            <br>
                            <br>
                            <input type="file" name="imagen" class="form-control-file" id="customFile" 
                                   accept=".jpeg, .png, .jpg">
                        </div>

                        <%}%>

                        <div style="text-align: center;">
                            <input class="btn btn-primary" style="width: 200px;" type="submit" > 
                        </div>
                    </form>
            </div>        
            <jsp:include page="jsps/footer.jspf" flush="true"/> 

        </div>


        <jsp:include page="jsps/scripts.jspf" flush="true"/>
    </body>

</html>
