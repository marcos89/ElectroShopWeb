<%-- 
    Document   : detalleProducto
    Created on : 02-ene-2018, 15:52:45
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ElectroShop</title>
    </head>
    <body style="background-color: azure">
        <div class="container-fluid">
            <jsp:include page="/jsps/header.jspf" flush="true"/>
            <nav   class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px;">Men&uacute;</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                        <li class="nav-item">
                            <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="InsertarCarrito?op=carrito" style="color: white;">Carrito</a>
                        </li>
                        <%
                            Usuario usuario = (Usuario) session.getAttribute("usuario");
                            if (usuario != null) {
                        %>
                        <li class="nav-item">    
                            <a class="nav-link" href="perfilUsuario" style="color: white;">Perfil</a>
                        </li>

                        <li class="nav-item">    
                            <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                        </li>

                        <%
                        } else {
                        %>
                        <li class="nav-item">
                            <a class="nav-link" href="login.jsp" style="color: white;">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="registro.jsp" style="color: white;">Registro</a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </nav>
            <!-- Tarjeta del producto -->
            <div class="row card" style="min-height: 750px; margin: 20px 0px 20px 0px; background-color: dodgerblue; border-radius: 10px;">
                <div class="col-sm-12">
                    <h1 style="color: white; text-align: center; margin-top: 10px;">Detalles del producto</h1>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div style="padding: 0px; margin-top: 10px;" class="col-sm-3">
                                <img class="card-img-top" style="min-width: 248px; width: 100%; height: 100%;border: 1px solid black; border-radius: 5px;" src="${producto.getImagen()}">
                            </div>
                            
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5" style="margin-top: 10px; padding-top: 20px; background-color: white; border: 1px solid black; border-radius: 5px;">
                                <h3 style="text-align:justify; margin: 25px 0px 50px 0px;">Categor&iacute;a: ${producto.getCategoria()} </h3>
                                <h3 style="text-align:justify; margin: 25px 0px 50px 0px;">Nombre: ${producto.getNombre()} </h3>
                                <h3 style="text-align:justify; margin: 25px 0px 50px 0px;">Precio: ${producto.getPrecio()} € </h3>
                            </div>    
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-10" style="background-color: white; min-height: 300px; border: 1px solid black; border-radius: 5px;">
                                <h3> Descripci&oacute;n: </h3> 
                             
                                <textarea style="font-size: 20px; border: none;resize: none; width:100%; margin-bottom: 20px" rows="10" readonly >${producto.getDescripcion()} </textarea>
                               
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4" style="text-align: center;"> 
                                <form  action="InsertarCarrito?op=insertar" method="post">
                                    <input type="hidden" name="id" value="${producto.getId()}">
                                    <input type="hidden" name="nombre" value="${producto.getNombre()}">
                                    <input type="hidden" name="precio" value="${producto.getPrecio()}">
                                    <input type="hidden" name="stock" value="${producto.getStock()}">
                                    <input class="btn btn-light" style="border: 1px solid black; background-color: azure;" type="submit" value="AÑADIR">
                                </form>
                            </div>
                            <div class="col-sm-4" ></div>
                        </div>


                    </div>
                
            </div>
            </div>

            <jsp:include page="jsps/footer.jspf" flush="true"/>
        
        
        </div>
        <jsp:include page="jsps/scripts.jspf" flush="true"/>

    </body>
</html>
