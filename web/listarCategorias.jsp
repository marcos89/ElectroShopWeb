<%-- 
    Document   : listarCategorias
    Created on : 10-ene-2018, 16:56:32
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <title>Admin</title>
    </head>
    <body style="background-color: azure">
        <% Usuario usuario = (Usuario) session.getAttribute("usuario");
            if (usuario == null) {
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
            }

        %>
            <div id="alert" class="alert alert-success" class="close" data-dismiss="alert" style="position: absolute; width: 50%; left: 25%;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>${msg}</strong>
            </div>
        <div class="container-fluid">
            <jsp:include page="jsps/header.jspf" flush="true"/>

            <nav   class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px;">Admin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">


                        <li class="nav-item">
                            <a class="nav-link" href="productos" style="color: white;"> Volver a Tienda </a>
                        </li>
                          <li class="nav-item">
                            <a class="nav-link" href="añadireditarcategoria.jsp" style="color: white;"> Añadir categorias </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="ListaProductos" style="color: white;"> Ver productos </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="VerVentas" style="color: white;"> Ver ventas </a>
                        </li>
                      
                            <%                    usuario = (Usuario) session.getAttribute("usuario");
                                if (usuario != null) {
                            %>
                            <li class="nav-item">
                                <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                            </li>
                            <% }%>


                    </ul>
                </div>
            </nav>

            <div class="table-responsive" style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">

                <br>
                <br>


                <table class="table table-striped" style="text-align: center;">
                    <thead>
                        <tr style="color: dodgerblue;">
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Editar</th>
                            <th scope="col">Eliminar </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${categorias}" var="categoria">
                            <tr>
                                <td>${categoria.getId()}</td>
                                <td>${categoria.getNombre()}</td>
                                
                                <td>
                                    <form action="añadireditarcategoria.jsp" method="post">
                                    <input type="hidden" name="id" value="${categoria.getId()}"/>    
                                    <input type="hidden" name="nombre" value="${categoria.getNombre()}"/>
                                    <input class="btn btn-primary" type="submit" value="Editar" />
                                </form>
                            </td>
                            <td >
                                <a class="btn btn-primary" href="EliminarCategoria?id=${categoria.getId()}">  

                                    Eliminar
                                </a>
                                


                                </td>

                            </tr>
                        </c:forEach>
                    </tbody>

                </table>
            </div>
            <jsp:include page="jsps/footer.jspf" flush="true"/>
        </div>

        <jsp:include page="jsps/scripts.jspf" flush="true"/>
        <script>
            function capturarid(id) {
                $("#id").val(id);
            }
        </script>   
        <script>
            if('${msg}' != "" && '${msg}' != null){
                window.onload = function() {
                    $(".alert").alert()
                    <% request.setAttribute("msg", ""); %>
                }
            }else{
                    $(".alert").alert('close')
            }

        </script>
    </body>


</html>
