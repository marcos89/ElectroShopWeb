<%-- 
    Document   : login
    Created on : 02-ene-2018, 19:28:01
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <title>ElectroShop</title>
        <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
    </head>
    <body style="background-color: azure">
        <div class="container-fluid">
            <% Usuario usuario = (Usuario) session.getAttribute("usuario");
                if (usuario != null) {
                    RequestDispatcher rd = request.getRequestDispatcher("productos");
                    rd.forward(request, response);
                }

            %>
            <div id="alert" class="alert alert-success" class="close" data-dismiss="alert" style="position: absolute; width: 50%; left: 25%;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>${msg}</strong>
            </div>
            <jsp:include page="jsps/header.jspf" flush="true"/>
                <nav class="navbar navbar-expand-lg navbar-light " style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigation">
                    <a class="navbar-brand" style="margin-left: 50px; color: white;">Men&uacute;</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                            <li class="nav-item">
                                <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="InsertarCarrito?op=carrito" style="color: white;">Carrito</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            <div style="min-height: 700px; margin: 20px 0px 20px 0px;">
                <form action="CheckLogin" method="post" style="padding: 15px; margin-top: 50px; border: 2px solid dodgerblue; border-radius: 10px;">
                    <div class="form-group">
                        <label style="font-weight:bold; font-size:20px; color: dodgerblue;"  for="exampleInputEmail1">Correo electrónico</label>
                        <input type="email" name="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >

                    </div>
                    <div class="form-group">
                        <label style="font-weight:bold; font-size:20px; color: dodgerblue;" for="exampleInputPassword1">Contraseña</label>
                        <input  type="password" name="pass" class="form-control" id="exampleInputPassword1" >
                    </div>
                    <div style="text-align: center; margin-top: 30px;">
                        <button type="submit" class="btn btn-primary" style="border: 1px solid black;">LOGIN</button>
                        <a href="registro.jsp" class="btn btn-primary" style="border: 1px solid black;">REGISTRARSE</a>
                    </div>
                </form>
            </div>
            <jsp:include page="jsps/footer.jspf" flush="true"/>
        </div>
        <jsp:include page="jsps/scripts.jspf" flush="true"/>
        <script>
            if('${msg}' != "" && '${msg}' != null){
                $(".alert").alert()
                <% request.setAttribute("msg", ""); %>
            }else{
                $(".alert").alert('close')
            }
        </script>
    </body>
</html>
