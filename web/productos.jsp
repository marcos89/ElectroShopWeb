<%-- 
    Document   : index
    Created on : 27-dic-2017, 15:39:43
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroShop</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="background-color: azure">
            
        <div class="container-fluid"> 
            <div id="alert" class="alert alert-success" class="close" data-dismiss="alert" style="position: absolute; width: 50%; left: 25%;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>${msg}</strong>
            </div>
            <jsp:include page="jsps/header.jspf" flush="true"/>
            <nav class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigation">
                <a class="navbar-brand" style="margin-left: 50px;">Men&uacute;</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="InsertarCarrito?op=carrito" style="color: white;">Carrito</a>
                        </li>

                        <li>
                            <%
                                Usuario usuario = (Usuario) session.getAttribute("usuario");
                                if (usuario != null) {
                            %>
                            <a class="nav-link" href="perfilUsuario" style="color: white;">Perfil</a>
                            <%
                                }
                            %>
                        </li>
                        <li class="nav-item">
                            <%
                                if (usuario != null) {
                            %>
                        <li class="nav-item">    
                            <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                        </li>
                        <%
                        } else {
                        %>
                        <li class="nav-item">
                            <a class="nav-link" href="login.jsp" style="color: white;">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="registro.jsp" style="color: white;">Registro</a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </nav>
            <div style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">
                <div class="form-group">
                    <label for="exampleFormControlSelect1" style="color: dodgerblue;">Seleccione categoría:</label>
                    <select class="form-control" id="select" onchange="selectCategoria()">
                        <option> - Categorías - </option>
                        <c:forEach items="${categorias}" var="categoria">
                            <c:if test="${categoria.getNombre() == categoriaSelec}">
                                <option selected>${categoria.getNombre()}</option>
                            </c:if>
                            <c:if test="${categoria.getNombre() != categoriaSelec}">
                                <option>${categoria.getNombre()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
                <% int index = 0; %>
                <c:forEach items="${productos}" var="producto" >
                    <% if (index % 4 == 0) { %>
                    <div class="row">
                        <% } %>
                        <div class="col-sm-3">
                            <div class="card" style=" background-color: dodgerblue; padding: 5px; margin-bottom: 10px; border: 2px solid black; border-radius: 5px;">
                                <img class="card-img-top" style="height: 200px;" src="${producto.getImagen()}">

                                <div class="card-block" style="padding: 5px;">
                                    <h4 class="card-title" style="color: white;">${producto.getNombre()}</h4>
                                    <h4 class="card-text" style="margin-bottom: 20px; color: white;">Precio: ${producto.getPrecio()}€</h4>
                                        <c:if test="${producto.getStock() == 0}">
                                            <h4 style="color: red; font-weight: bold;">Producto agotado</h4>
                                        </c:if>
                                        <c:if test="${producto.getStock() > 0}">
                                      
                                        <a style="text-align: center; margin-top: 5px; display: block; margin-left: auto; margin-right: auto; width: 120px;background-color: azure; border: 1px solid black; " class="btn btn-light" href="detalleProducto?id=${producto.getId()}" >DETALLES</a>
                                    
                                    
                                        <form action="InsertarCarrito" method="post">
                                            <input type="hidden" name="op" value="insertar">
                                            <input type="hidden" name="id" value="${producto.getId()}">
                                            <input type="hidden" name="nombre" value="${producto.getNombre()}">
                                            <input type="hidden" name="precio" value="${producto.getPrecio()}">
                                            <input type="hidden" name="stock" value="${producto.getStock()}">
                                            <input type="hidden" name="descripcion" value="${producto.getDescripcion()}">
                                            <input class="btn btn-light" style="min-width: 120px; background-color: azure; border: 1px solid black;text-align: center; margin-top: 5px;  display: block; margin-left: auto; margin-right: auto;" type="submit" value="AÑADIR">
                                        </form>
                                      
                                        </c:if>
                                </div>
                            </div>
                        </div>
                        <% if (index % 4 == 3) { %>
                    </div>
                    <% }
                        index++;
                    %>
                </c:forEach>
                <% index--;
                    if (index % 4 != 3) { %>
            </div>
            <% }
            %>

        </div>
        <jsp:include page="jsps/footer.jspf" flush="true"/>
        <jsp:include page="jsps/scripts.jspf" flush="true"/>
        <script>
            if ('${msg}' != "" && '${msg}' != null) {
                $(".alert").alert();
            <% request.setAttribute("msg", "");%>
            } else {
                $(".alert").alert('close');
            }
            function selectCategoria() {
                categoria = $("#select").val();
                window.location.href = "productos?categoria=" + categoria;
            }
        </script>
    </body>
</html>
