<%-- 
    Document   : profile
    Created on : 04-ene-2018, 16:35:24
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ElectroShop</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="background-color: azure">
        <div class="container-fluid">
            <%
                Usuario usuario = (Usuario) session.getAttribute("usuario");
                if (usuario == null) {
                    RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                    rd.forward(request, response);
                }
            %>
            <div id="alert" class="alert alert-success" class="close" data-dismiss="alert" style="position: absolute; width: 50%; left: 25%;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>${msg}</strong>
            </div>
            <jsp:include page="/jsps/header.jspf" flush="true"/>
            <nav   class="navbar navbar-expand-lg navbar-light " style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px; color: white;">Men&uacute;</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                        <li class="nav-item">
                            <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="InsertarCarrito?op=carrito" style="color: white;">Carrito</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                        </li>
                    </ul>  
                </div>
            </nav>
            <div style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">
                <div class="row my-2">
                    <div class="col-lg-12  order-lg-2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Perfil</a>
                            </li>
                            <li class="nav-item">
                                <a href="" data-target="#pedidos" data-toggle="tab" class="nav-link">Pedidos</a>
                            </li>
                            <li class="nav-item">
                                <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Editar</a>
                            </li>
                            <li class="nav-item">
                                <a href="" data-target="#pass" data-toggle="tab" class="nav-link">Cambiar contraseña</a>
                            </li>
                            <%  System.out.println(usuario.getRol());
                                if (usuario.getRol().equals("Administrador")) { %>
                            <li class="nav-item">
                                <a href="" data-target="#admin" data-toggle="tab" class="nav-link">Administrador</a>
                            </li>
                            <% }%>
                        </ul>
                        <div class="tab-content py-4">
                            <div class="tab-pane active" id="profile">
                                <h5 class="mb-3" style="color: dodgerblue">Perfil de Usuario</h5>
                                <p>${usuario.getNombre()} ${usuario.getApellidos()}</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="color: dodgerblue">Direccion</h6>
                                        <p>
                                            ${usuario.getDireccion()}
                                        </p>
                                        <h6 style="color: dodgerblue">Telefono</h6>
                                        <p>
                                            ${usuario.getTelefono()}
                                        </p>
                                        <h6 style="color: dodgerblue">email</h6>
                                        <p>
                                            ${usuario.getMail()}
                                        </p>

                                    </div>


                                </div>
                                <!--/row-->
                            </div>
                            <div class="tab-pane table-responsive" style="display: box;" id="pedidos">

                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr style="color: dodgerblue;">
                                            <th>#Pedido</th>
                                            <th>Fecha</th>
                                            <th>Precio Total</th>
                                            <th>Ver pedido</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                        <c:forEach items="${pedidos}" var="pedido" >                                  
                                            <tr>
                                                <td>
                                                    ${pedido.getIdPedido()}
                                                </td>
                                                <td>
                                                    ${pedido.getFecha()}
                                                </td>
                                                <td>
                                                    ${pedido.getCarrito().getPrecioTotal()}
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary" onclick="showDiv(${pedido.getIdPedido()})">Ver pedido</button>
                                                </td>

                                            </tr>
                                            <tr  id="products${pedido.getIdPedido()}" style="display: none;">
                                                <td colspan="3">
                                                    <div class="table-responsive">
                                                    <table class="table table-hover table-striped" style="text-align:center;">
                                                        <thead>
                                                            <tr style="color: dodgerblue;">
                                                                <th>Cantidad</th>
                                                                <th>Producto</th>
                                                                <th>Gasto</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody> 
                                                            <c:forEach items="${pedido.getCarrito().getProductos()}" var="producto" >  
                                                                <tr>
                                                                    <td>${producto.getCantidad()}</td><td>${producto.getProducto().getNombre()}</td><td>${producto.getProducto().getPrecio()*producto.getCantidad()}€</td>
                                                                </tr>
                                                            </c:forEach>
                                                            <tr>
                                                                <td>1</td><td>Envío</td><td>6€</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                </td>
                                            </tr>

                                        </c:forEach>
                                    </tbody> 
                                </table>

                            </div>
                            <div class="tab-pane" id="edit">
                                <form method="post" action="editarUsuario" >
                                    <input type="hidden" name="id" value="${usuario.getId()}" class="form-control"  >
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Nombre</label>
                                        <input type="text" name="nombre" pattern="[A-Za-z]{}" value="${usuario.getNombre()}"  class="form-control"  >

                                    </div>
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Apellido</label>
                                        <input type="text" name="apellido" pattern="[A-Za-z]{}" value="${usuario.getApellidos()}" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Direcci&oacute;n</label>
                                        <input type="text" name="direccion" value="${usuario.getDireccion()}" class="form-control"  >
                                    </div>
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Tel&eacute;fono</label>
                                        <input type="text" name="telefono"  value="${usuario.getTelefono()}"class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label style="color: dodgerblue">E-mail</label>
                                        <input type="email" name="mail" value="${usuario.getMail()}" class="form-control"  >
                                    </div>
                                    <div style="text-align: center;">
                                        <button type="submit" class="btn btn-primary" style="border: 1px solid black;">Editar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="pass">
                                <form method="post" action="CambioPass" >
                                    <input type="hidden" name="id" value="${usuario.getId()}" class="form-control"  >
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Actual contraseña</label>
                                        <input type="password" name="currpass" class="form-control" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="color: dodgerblue">Nueva contraseña</label>
                                        <input type="password" name="newpass" class="form-control" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required>
                                    </div>
                                    <div style="text-align: center;">
                                        <button type="submit" class="btn btn-primary" style="border: 1px solid black">Cambiar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="admin">
                                <a class="btn btn-primary" href="ListaProductos" style="border: 1px solid black">Gestión de tienda</a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
    <jsp:include page="jsps/footer.jspf" flush="true"/>
</div>
</body>
<jsp:include page="jsps/scripts.jspf" flush="true"/>
    <script>
        if('${msg}' != "" && '${msg}' != null){
            $(".alert").alert()
            <% request.setAttribute("msg", ""); %>
        }else{
            $(".alert").alert('close')
        }

        function showDiv(id) {

            $("#products" + id).toggle();

        }
    </script>
</html>
