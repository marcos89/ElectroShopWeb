<%-- 
    Document   : registro.jsp
    Created on : 22-dic-2017, 19:23:14
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <!doctype html>
    <html lang="en">

        <head>
            <title>ElectroShop</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        </head>
        <body style="background-color: azure">
            <div class="container-fluid">
                <% Usuario usuario = (Usuario) session.getAttribute("usuario");
                    if (usuario != null) {
                        RequestDispatcher rd = request.getRequestDispatcher("productos");
                        rd.forward(request, response);
                    }

                %>
                <div id="alert" class="alert alert-success" class="close" data-dismiss="alert" style="position: absolute; width: 50%; left: 25%;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>${msg}</strong>
                </div>
                <jsp:include page="/jsps/header.jspf" flush="true"/>
                <nav class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigation">
                    <a class="navbar-brand" style="margin-left: 50px;">Men&uacute;</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="InsertarCarrito?op=carrito" style="color: white;">Carrito</a>
                            </li>
                        </ul>
                    </div> 
                </nav> <br>

                <div style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">
                    <h1 style="text-align: center; color: dodgerblue;"> Registro </h1>

                    <form method="post" action="RegistroUsuario" >
                        <div class="form-group">
                            <label style="color: dodgerblue;">Nombre</label>
                            <input type="text" name="nombre" pattern="[A-Za-z]{}" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Apellido</label>
                            <input type="text" name="apellido" pattern="[A-Za-z]{}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Dirección</label>
                            <input type="text" name="direccion" class="form-control" required>
                        </div>
                        <div>
                            <label style="color: dodgerblue;">Tel&eacute;fono</label>
                            <input type="text" name="telefono" pattern="[0-9]{9}" class="form-control" maxlength="9" required>
                            <small style="color: red;">El número de teléfono ha de contener 9 números consecutivos del 0 al 9.</small>
                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">E-mail</label>
                            <input type="email" name="mail"class="form-control" required>
                            <small style="color: red;">La cuenta de correo electrónico ha de ser una cuenta válida.</small>
                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Contraseña</label>
                            <input type="password" name="pass" class="form-control" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required>
                            <small style="color: red;">La contraseña ha de contener al menos una letra mayúscula, minúscula y un número.</small>
                        </div>
                        <div class="form-group">
                            <label style="color: dodgerblue;">Repite contraseña</label>
                            <input type="password" name="repass" class="form-control" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" required>
                                Estoy de acuerdo en que mis datos sean almacenados                       
                            </label>
                        </div>
                        <div style="text-align: center; margin-top: 30px;">
                            <button type="submit" class="btn btn-primary">REGISTRAR</button>
                        </div>
                    </form>
              
                </div>
                <jsp:include page="jsps/footer.jspf" flush="true"/>
            </div>
            <jsp:include page="jsps/scripts.jspf" flush="true"/>
                  <script>
                        if('${msg}' != "" && '${msg}' != null){
                            $(".alert").alert()
                            <% request.setAttribute("msg", ""); %>
                        }else{
                            $(".alert").alert('close')
                        }
                    </script>
        </body>
    </html>
