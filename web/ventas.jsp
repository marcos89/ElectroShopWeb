<%-- 
    Document   : ventas
    Created on : 10-ene-2018, 17:23:44
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <title>Admin</title>
    </head>
    <body style="background-color: azure">
        <% Usuario usuario = (Usuario) session.getAttribute("usuario");
            if (usuario == null) {
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
            }

        %>


        <div class="container-fluid">
            <jsp:include page="jsps/header.jspf" flush="true"/>

            <nav class="navbar navbar-expand-lg navbar-light" style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand" style="margin-left: 50px;">Admin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">


                        <li class="nav-item">
                            <a class="nav-link" href="productos" style="color: white;"> Volver a tienda </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ListaProductos" style="color: white;"> Ver productos </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ListaCategorias" style="color: white;"> Ver categor&iacute;as </a>
                        </li>
                    

                            <%                    usuario = (Usuario) session.getAttribute("usuario");
                                if (usuario != null) {
                            %>
                            <li class="nav-item">
                            <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                            </li>
                            <% }%>

                        

                    </ul>
                </div>
            </nav>

            <div  style="min-height: 750px; margin: 20px 0px 20px 0px; border: 2px solid dodgerblue; padding: 15px; border-radius: 10px;">

                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped" style="text-align: center;">
                        <thead>
                            <tr style="color: dodgerblue;">
                                <th scope="col">#</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Ver pedido</th>


                            </tr>
                        </thead>
                        <tbody> 
                            <c:forEach items="${pedidos}" var="pedido" >

                                <tr>
                                    <td>${pedido.getIdPedido()}</td>
                                    <td>${pedido.getIdUsuario()}</td>
                                    <td>${pedido.getFecha()}</td>
                                    <td>${pedido.getCarrito().getPrecioTotal()}</td>
                                    <td >
                                        <button type="button" onclick="showDiv(${pedido.getIdPedido()})"    class="btn btn-primary" data-toggle="modal" data-target="#Eliminar">  

                                            Ver pedido
                                        </button>
                                    </td>
                                </tr>
                                <tr  id="products${pedido.getIdPedido()}" style="display: none;">
                                    <td colspan="4">
                                        <table class="table table-hover table-striped" style="text-align:center;">
                                            <thead>
                                                <tr style="color: dodgerblue;">
                                                    <th>Cantidad</th>
                                                    <th>Producto</th>
                                                    <th>Gasto</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <c:forEach items="${pedido.getCarrito().getProductos()}" var="producto" >  
                                                    <tr>
                                                        <td>${producto.getCantidad()}</td><td>${producto.getProducto().getNombre()}</td><td>${producto.getProducto().getPrecio()*producto.getCantidad()}€</td>
                                                    </tr>
                                                </c:forEach>
                                                <tr>
                                                    <td>1</td><td>Envío</td><td>6€</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>

                    </table>
                </div>
            </div>
            <jsp:include page="jsps/footer.jspf" flush="true"/>
        </div>
    
        <jsp:include page="jsps/scripts.jspf" flush="true"/>
            <script>
            function showDiv(id) {

                $("#products" + id).toggle();

            }
        </script>

    </body>


</html>
