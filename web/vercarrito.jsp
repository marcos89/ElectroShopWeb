<%-- 
    Document   : carrito
    Created on : 22-dic-2017, 19:34:04
    Author     : Grupo2
--%>

<%@page import="Clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ElectroShop</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    </head>
     <body style="background-color: azure"> 
    <!--<body  style="background-color: green"> -->
        <div class="container-fluid" style="text-align: center;">

            <jsp:include page="jsps/header.jspf" flush="true"/>
            <nav   class="navbar navbar-expand-lg navbar-light " style="background-color: dodgerblue; padding: 5px; color: white; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px" role="navigator">
                <a class="navbar-brand"  style="margin-left: 50px; color: white;">Men&uacute;</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                        <li class="nav-item">
                            <a class="nav-link" href="productos" style="color: white;">Volver a tienda</a>
                        </li>

                        <li>
                            <%
                                Usuario usuario = (Usuario) session.getAttribute("usuario");
                                if (usuario != null) {
                            %>
                            <a class="nav-link" href="perfilUsuario" style="color: white;">Perfil</a>
                            <%
                                }
                            %>
                        </li>
                        <li class="nav-item">
                            <%
                                usuario = (Usuario) session.getAttribute("usuario");
                                if (usuario != null) {
                            %>
                        <li class="nav-item">    
                            <a class="nav-link" href="Logout" style="color: white;">Logout</a>
                        </li>
                        <%
                        } else {
                        %>
                        <li class="nav-item">
                            <a class="nav-link" href="login.jsp" style="color: white;">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="registro.jsp" style="color: white;">Registro</a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
           </nav>
            <div style="min-height: 750px; border: 2px solid dodgerblue; border-radius: 10px; margin: 20px 0px 20px 0px; padding: 10px;">
            <h1 style="margin-bottom: 30px; color: dodgerblue;">Carrito de productos</h1>
            <div class="table-responsive">   
                <table class="table table-striped" style="border-radius: 5px; text-align: center;">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Producto</th>
                            <th scope="col">Precio Unidad</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Precio Total</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <% int numProducto = 1;%>
                        <c:forEach items="${carrito.getProductos()}" var="producto" >
                            <tr>
                                <td><%= numProducto%></td>
                                <td>${producto.getProducto().getNombre()}</td>
                                <td>${producto.getProducto().getPrecio()} €</td>
                                <td>${producto.getCantidad()}</td>
                                <td>${producto.getPrecioCantidad()} €</td>
                            </tr>
                            <% numProducto++; %>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Precio compra</td>
                            <td>${carrito.getPrecioTotal() - 6} €</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Precio envío</td>
                            <td>6 €</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Precio total</td>
                            <td>${carrito.getPrecioTotal()} €</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div>
                <a class="btn btn-primary" style="width: 200px; margin-top: 30px; color: white; border: 1px solid black;" href="VaciarCarrito">VACIAR CARRITO</a>        
                <% if (usuario == null) { %>
                <button class="btn btn-primary" style="width: 200px; margin-top: 30px; color: white; border: 1px solid black;" type="button" onclick="noLogin()">COMPRAR</button>
                <% } else { %>
                <form style="text-align:center; margin-bottom: 30px;" action="RegistroPedido" method="POST">
                    <input class="btn btn-primary" style="width: 200px; margin-top: 30px; color: white; border: 1px solid black;" type="submit" value="COMPRAR">
                </form>
                <% }%>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        </div>
                        <div class="modal-body">
                            Es necesario estar registrado para realizar un pedido.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
         
            </div>
            <jsp:include page="jsps/footer.jspf" flush="true"/>
        </div>
       
        <jsp:include page="jsps/scripts.jspf" flush="true"/>
           <script>
                function noLogin() {
                    $("#myModal").modal("show")
                }
            </script>
    </body>
</html>
